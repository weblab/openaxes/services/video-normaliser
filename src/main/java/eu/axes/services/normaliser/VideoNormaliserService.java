package eu.axes.services.normaliser;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import javax.jws.WebService;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.content.api.ContentManager;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;
import org.purl.dc.elements.DublinCoreAnnotator;

import eu.axes.components.contentmanager.AxesContentManager;

/**
 * The video normaliser is a service in charge of generating converted version of the native content video file annotated on the input document. Then the generate files will be annotated (if needed)
 * to the document so that the next services in the processing chain can access them if needed.
 *
 * @author ymombrun
 * @date 2014-11-21
 */
@WebService(endpointInterface = "org.ow2.weblab.core.services.Analyser")
public class VideoNormaliserService implements Analyser {


	protected AxesFFmpegNormaliser axesFFmpegNormaliser;


	protected AxesContentManager axesContentManager;


	protected ContentManager contentManager;


	protected Map<String, String> extMimeMap;


	protected final Log log;


	private final File tempFolder;


	/**
	 * Generate an AxesNormaliserService with default configuration.
	 *
	 * @param ffmpeg
	 *            The path FFmpeg executable
	 * @param tempFolder
	 *            The path to the temporary folder where content will be generated
	 *
	 * @throws IOException
	 *             If the normaliser cannot be instantiated due to errors in files configuration
	 */
	public VideoNormaliserService(final String ffmpeg, final String tempFolder) throws IOException {
		this(ffmpeg, new File(ffmpeg).getParent(), tempFolder);
	}


	/**
	 * Generate an AxesNormaliserService with default configuration.
	 *
	 * @param ffmpeg
	 *            The path or name of FFmpeg executable
	 * @param workingDirectory
	 *            The path of the working directory
	 * @param tempFolder
	 *            The path to the temporary folder where content will be generated
	 *
	 * @throws IOException
	 *             If the normaliser cannot be instantiated due to errors in files configuration
	 */
	public VideoNormaliserService(final String ffmpeg, final String workingDirectory, final String tempFolder) throws IOException {
		super();
		this.log = LogFactory.getLog(this.getClass());
		this.axesFFmpegNormaliser = new AxesFFmpegNormaliser(ffmpeg, workingDirectory);
		this.axesContentManager = new AxesContentManager();
		this.contentManager = ContentManager.getInstance();
		this.extMimeMap = new HashMap<>();
		this.tempFolder = new File(tempFolder);
		FileUtils.forceMkdir(this.tempFolder);

		/*
		 * populate map (reverse of content manager map)
		 */
		for (final Entry<String, String> mimeEntry : this.axesContentManager.getFormatToExtMap().entrySet()) {
			this.extMimeMap.put(mimeEntry.getValue(), mimeEntry.getKey());
		}
	}



	@Override
	public ProcessReturn process(final ProcessArgs args) throws UnexpectedException, ContentNotAvailableException, InvalidParameterException {
		this.log.trace("Process method called.");

		final File nativeContentFile = this.checkArgs(args);
		this.log.debug("Normaliser called for video " + args.getResource().getUri() + ".");

		final String tempFilePrefix = Calendar.getInstance().getTime().getTime() + "_" + UUID.randomUUID().toString();

		/*
		 * for each configured format, generate a normalised content
		 */
		for (final Entry<String, String> commandEntry : this.axesFFmpegNormaliser.getVideoCmdLines().entrySet()) {
			try {
				this.generateNormalisedContent(nativeContentFile, args.getResource(), commandEntry.getKey(), tempFilePrefix);
				this.generateNormalisedContent(nativeContentFile, ((Document) args.getResource()).getMediaUnit().get(0), commandEntry.getKey(), tempFilePrefix);
			} catch (final Exception e) {
				final String msg = "Unable to convert file " + nativeContentFile.getAbsolutePath() + " from resource " + args.getResource().getUri() + " into a ." + commandEntry.getKey() + ".";
				this.log.error(msg, e);
				throw ExceptionFactory.createUnexpectedException(msg, e);
			}
		}

		this.log.debug("End of normaliser for video " + args.getResource().getUri() + ".");
		final ProcessReturn processReturn = new ProcessReturn();
		processReturn.setResource(args.getResource());
		return processReturn;
	}


	/**
	 * @param args
	 *            The process args received
	 * @return The native content file
	 * @throws ContentNotAvailableException
	 *             If no native content annotation is found or if the file does not exist.
	 * @throws InvalidParameterException
	 *             If args is null or if the resource inside is null or not a document.
	 */
	private File checkArgs(final ProcessArgs args) throws ContentNotAvailableException, InvalidParameterException {
		if (args == null) {
			final String message = "ProcessArgs was null.";
			this.log.error(message);
			throw ExceptionFactory.createInvalidParameterException(message);
		}
		final Resource res = args.getResource();
		if (res == null) {
			final String message = "Resource in ProcessArgs was null.";
			this.log.error(message);
			throw ExceptionFactory.createInvalidParameterException(message);
		}
		if (!(res instanceof Document)) {
			final String message = "Resource in ProcessArgs was not a document.";
			this.log.error(message);
			throw ExceptionFactory.createInvalidParameterException(message);
		}

		final File nativeContentFile;
		try {
			nativeContentFile = this.contentManager.readNativeContent(args.getResource());
		} catch (final WebLabCheckedException wlce) {
			final String msg = "Unable to get native content for resource " + args.getResource().getUri() + ".";
			this.log.error(msg, wlce);
			throw ExceptionFactory.createContentNotAvailableException(msg, wlce);
		}
		return nativeContentFile;
	}


	/**
	 * Add a normalised content to a resource using a format.
	 *
	 * @param nativeContentFile
	 *            Native file used to generate normalized content
	 * @param resource
	 *            resource to annotate
	 * @param formatKey
	 *            format key used to generate content
	 * @throws WebLabCheckedException
	 *             If the expected output format is not handled by the axes conten manager.
	 * @throws IOException
	 *             If an error occurred when calling the command line
	 */
	private void generateNormalisedContent(final File nativeContentFile, final Resource resource, final String formatKey, final String tempFilePrefix) throws WebLabCheckedException, IOException {
		final String mimeTypeOutput = this.extMimeMap.get(formatKey);
		final URI normalisedContentUri = this.axesContentManager.getContentUri(resource, formatKey);
		final File normalisedFile = this.axesContentManager.getFilePath(normalisedContentUri);

		final boolean needAnnotation;
		if (normalisedFile.equals(nativeContentFile)) {
			needAnnotation = true;
		} else {
			// Create a tempVideoFolder based on collection and video names from the path to the normalised file (each video is stored in collectionId/videoId/videoId.ext file)
			final File tempVideoFolder = new File(new File(this.tempFolder, normalisedFile.getParentFile().getParentFile().getName()), normalisedFile.getParentFile().getName());
			FileUtils.forceMkdir(tempVideoFolder);
			final File tempDestFile = new File(tempVideoFolder, normalisedFile.getName());

			final String mimeTypeInput = new DublinCoreAnnotator(resource).readFormat().firstTypedValue();
			needAnnotation = this.axesFFmpegNormaliser.launchCommand(nativeContentFile.getAbsolutePath(), tempDestFile.getAbsolutePath(), formatKey, mimeTypeInput, tempFilePrefix);
			if (needAnnotation && ((!normalisedFile.exists()) || (!FileUtils.contentEquals(normalisedFile, tempDestFile)))) {
				FileUtils.copyFile(tempDestFile, normalisedFile);
			}
		}
		if (needAnnotation) {
			final WProcessingAnnotator wProcessingAnnotator = new WProcessingAnnotator(resource);
			final PieceOfKnowledge pok = wProcessingAnnotator.writeNormalisedContent(normalisedContentUri);
			final DublinCoreAnnotator dublinCoreAnnotator = new DublinCoreAnnotator(normalisedContentUri, pok);
			/*
			 * If this content URI is not already annotated with type (in the case of native URI = normalised URI)
			 */
			if (!dublinCoreAnnotator.readFormat().hasValue() && (mimeTypeOutput != null)) {
				dublinCoreAnnotator.writeFormat(mimeTypeOutput);
			}
		}
	}

}
