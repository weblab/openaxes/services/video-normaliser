package eu.axes.services.normaliser;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.Executor;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * The actual component in charge of generating the command line and to call it from its configuration file.
 *
 * @author vbruel
 */
public class AxesFFmpegNormaliser {


	private static final String DEFAULT_TIMEOUT = String.valueOf(10 * 60 * 60 * 1000);


	private static final String DEFAULT_AXES_FFMPEG_NORMALISER_PROPERTIES = "axesFfmpegNormaliser.properties";


	private static final String COMMAND_KEY = "command";


	private static final String FORMATS_KEY = "formats";


	private static final String SKIP_CONVERT_TO = "skipConvertTo";


	private static final String IF_MIME_IS_ONE_OF = "ifMimeTypeIsOneOf";


	private static final String IF_MIME_IS_NOT_ONE_OF = "ifMimeTypeIsNotOneOf";


	private static final String TIMEOUT = "timeout";


	private final LinkedHashMap<String, String> videoCmdLines;


	private final Properties props;


	private final Log logger = LogFactory.getLog(this.getClass());


	private final long timeout;


	private final File workingDirectory;


	private final String ffmpeg;


	/**
	 * Constructor from a property file stream. Loads the map from this property file.
	 *
	 * @param ffmpegPath
	 *            FFmpeg executable path or name.
	 * @param workingDirectory
	 *            The path to the working directory
	 * @param propertyStream
	 *            stream to property file
	 * @throws IOException
	 *             Error during property stream reading or if paths are bad
	 */
	public AxesFFmpegNormaliser(final String ffmpegPath, final String workingDirectory, final InputStream propertyStream) throws IOException {
		super();
		this.videoCmdLines = new LinkedHashMap<>();
		this.ffmpeg = ffmpegPath;
		
		/*
		 * load properties
		 */
		this.props = new Properties();
		this.props.load(propertyStream);

		/*
		 * list needed formats
		 */
		final String formats = this.props.getProperty(AxesFFmpegNormaliser.FORMATS_KEY);
		if ((formats == null) || (formats.trim().length() == 0)) {
			final String message = "Unable to find key: " + AxesFFmpegNormaliser.FORMATS_KEY + " in properties.";
			this.logger.fatal(message);
			throw new IOException(message);
		}

		/*
		 * get command line for each format
		 */
		for (final String format : formats.split(",")) {
			final String trimedFormat = format.trim();
			final String commandLine = this.props.getProperty(AxesFFmpegNormaliser.COMMAND_KEY + "." + trimedFormat);
			if ((commandLine == null) || (commandLine.trim().length() == 0)) {
				throw new IOException("Unable to find key: " + AxesFFmpegNormaliser.COMMAND_KEY + "." + trimedFormat + " in properties.");
			}
			this.logger.debug("Adding command line: '" + commandLine + "' for format: " + trimedFormat);
			this.videoCmdLines.put(trimedFormat, commandLine);
		}

		this.timeout = Long.valueOf(this.props.getProperty(AxesFFmpegNormaliser.TIMEOUT, AxesFFmpegNormaliser.DEFAULT_TIMEOUT)).longValue();

		this.workingDirectory = new File(workingDirectory);
		if ((this.workingDirectory.exists() && !this.workingDirectory.isDirectory()) || (!this.workingDirectory.exists() && !this.workingDirectory.mkdirs())) {
			final String message = "Directory does not exist, cannot be created or is not a directory " + this.workingDirectory.getAbsolutePath() + ".";
			this.logger.fatal(message);
			throw new IOException(message);
		}

		this.logger.info("Normaliser command lines: " + this.videoCmdLines);
	}


	/**
	 * Search for default property in class loader to instantiate the normaliser.
	 *
	 * @param ffmpegPath
	 *            Path or name of FFMPEG executable.
	 * @param workingDirectory
	 *            The path to the working directory.
	 * @see AxesFFmpegNormaliser#AxesFFmpegNormaliser(String,String,InputStream)
	 * @throws IOException
	 *             Something wrong during properties loading or if path are bad.
	 */
	public AxesFFmpegNormaliser(final String ffmpegPath, final String workingDirectory) throws IOException {
		this(ffmpegPath, workingDirectory, AxesFFmpegNormaliser.class.getClassLoader().getResourceAsStream(AxesFFmpegNormaliser.DEFAULT_AXES_FFMPEG_NORMALISER_PROPERTIES));
	}



	/**
	 * Launch the avconv/ffmpeg -i nativeFile command with destination type specific command line part. Generate destination file.
	 *
	 * @param inputFilePath
	 *            file used as input for avconv command line
	 * @param outputFilePath
	 *            output file written by avconv command line
	 * @param destType
	 *            output type (used to use the right command line options)
	 * @param mimeType
	 *            The mime type of the input video
	 * @param tempFileNamePrefix
	 *            Prefix of the temporary file to be generated
	 * @return false if conversion has been skipped as not necessary
	 * @throws IOException
	 *             If something wrong when trying to invoke the command line
	 */
	public boolean launchCommand(final String inputFilePath, final String outputFilePath, final String destType, final String mimeType, final String tempFileNamePrefix) throws IOException {
		final File destinationFile = new File(outputFilePath);
		if (destinationFile.exists()) {
			this.logger.debug("Skipping processing, file '" + outputFilePath + "' already exists.");
			return true;
		}

		if (!this.videoCmdLines.containsKey(destType)) {
			throw new IOException("Unrecognised output type: " + destType + ". Must be configured in axes normaliser property file.");
		}

		// "video/ts" to "video-ts"
		final String videoMimeSuffix = mimeType == null ? "no-mime-type" : mimeType.replace('/', '-');

		// Check if this conversion has to be skipped:
		// skipConvertTo.<destType>.ifMimeTypeIsOneOf=<videoMimeSuffix>,othervideomimesuffix
		// skipConvertTo.<destType>.ifMimeTypeIsNotOneOf=<videoMimeSuffix>,othervideomimesuffix
		if (this.skipConvertToIfMimeIsOneOf(destType, videoMimeSuffix) || this.skipConvertToIfMimeIsNotOneOf(destType, videoMimeSuffix)) {
			return false;
		}

		// Does a specific command exist? command.mp4.video-ts=...
		final String propertySpecific = AxesFFmpegNormaliser.COMMAND_KEY + "." + destType + "." + videoMimeSuffix;
		String commandLine = this.props.getProperty(propertySpecific);
		if ((commandLine != null) && (commandLine.trim().length() > 0)) {
			this.logger.debug("EXISTING specific property: " + propertySpecific);
		} else {
			this.logger.debug("NOT EXISTING specific property: " + propertySpecific);
			commandLine = this.videoCmdLines.get(destType);
		}

		final List<String> commandInitial = AxesFFmpegNormaliser.splitAndTrim(commandLine);
		final List<String> commandFinal = new ArrayList<>();
		commandFinal.add(this.ffmpeg);

		// Replace %xxx% variables in the command line by:
		// -computed replacement:
		// -ex: %temp% => temp file name prefix
		// -ex: %n% => nativeFilePath
		// -ex: %d% => destinationFilePath
		// -existing property replacement (ex: %dbg% => a property named "dbg" MUST exist in the property file)
		for (final String str : commandInitial) {
			final List<String> commandTmp = new ArrayList<>();
			this.replaceRecursively(str, commandTmp, tempFileNamePrefix, inputFilePath, outputFilePath);
			commandFinal.addAll(commandTmp);
		}

		final CommandLine cmdLine = CommandLine.parse(StringUtils.join(commandFinal, ' '));

		// Display command line as a single string (to permit copy/paste to try in a terminal)
		this.logger.info("Trying to launch command line: " + cmdLine.toString());

		final DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
		final ExecuteWatchdog watchdog = new ExecuteWatchdog(this.timeout);

		try (final ByteArrayOutputStream baos = new ByteArrayOutputStream();) {
			try {
				final Executor executor = new DefaultExecutor();
				executor.setWatchdog(watchdog);
				executor.setExitValue(0);
				executor.setWorkingDirectory(this.workingDirectory);
				executor.setStreamHandler(new PumpStreamHandler(baos));

				this.logger.debug("Executing commandline " + cmdLine.toString());

				executor.execute(cmdLine, resultHandler);

				try {
					resultHandler.waitFor();
				} catch (final InterruptedException ie) {
					throw new IOException("Unable to wait for the end of command line " + cmdLine.toString() + ".", ie);
				}
			} finally {
				try {
					final String logFileName = FilenameUtils.getBaseName(this.ffmpeg) + '-' + FilenameUtils.getExtension(inputFilePath) + '-' + FilenameUtils.getExtension(outputFilePath) + ".log";
					FileUtils.write(new File(new File(outputFilePath).getParentFile(), logFileName), baos.toString("utf-8"), "utf-8");
				} catch (final IOException ioe) {
					this.logger.warn("An error occurs writing output of " + cmdLine.toString() + " into a log file.", ioe);
				}
			}
		} catch (final IOException ioe) {
			this.logger.warn("An error occurs closing output stream of " + cmdLine.getExecutable() + ".", ioe);
		}

		this.logger.debug("Commandline " + cmdLine.toString() + " has been successfully executed.");

		return true;
	}


	private boolean skipConvertToIfMimeIsOneOf(final String destType, final String videoMimeSuffix) {
		final String propkey1 = AxesFFmpegNormaliser.SKIP_CONVERT_TO + "." + destType + "." + AxesFFmpegNormaliser.IF_MIME_IS_ONE_OF;
		final String propvalue1 = this.props.getProperty(propkey1);
		if ((propvalue1 != null) && (propvalue1.trim().length() > 0)) {
			final String[] tab1 = propvalue1.trim().split(",");
			for (final String str1 : tab1) {
				if (str1.equalsIgnoreCase(videoMimeSuffix)) {
					return true;
				}
			}
		} else {
			this.logger.trace("NOT EXIST property " + propkey1);
		}
		return false;
	}


	private boolean skipConvertToIfMimeIsNotOneOf(final String destType, final String videoMimeSuffix) {
		final String propkey2 = AxesFFmpegNormaliser.SKIP_CONVERT_TO + "." + destType + "." + AxesFFmpegNormaliser.IF_MIME_IS_NOT_ONE_OF;
		final String propvalue2 = this.props.getProperty(propkey2);
		if ((propvalue2 != null) && (propvalue2.trim().length() > 0)) {
			final String[] tab2 = propvalue2.trim().split(",");
			boolean mimeIsDedans = false;
			for (final String str2 : tab2) {
				if (str2.equalsIgnoreCase(videoMimeSuffix)) {
					mimeIsDedans = true;
				}
			}
			if (!mimeIsDedans) {
				this.logger.trace("Skipping conversion to " + destType + " because property IF_MIME_IS_NOT_ONE_OF exists:" + propkey2 + "=" + propvalue2);
				return true;
			}
		} else {
			this.logger.trace("NOT EXIST property " + propkey2);
		}
		return false;
	}


	private void replaceRecursively(final String str, final List<String> output, final String tempFileNamePrefix, final String nativeFilePath, final String destinationFilePath) {
		String str3 = null;
		if (str.startsWith("%temp%")) {
			str3 = str.replaceAll(Pattern.quote("%temp%"), tempFileNamePrefix);
		} else if (str.indexOf("%n%") >= 0) {
			str3 = str.replaceAll(Pattern.quote("%n%"), nativeFilePath);
		} else if (str.indexOf("%d%") >= 0) {
			str3 = str.replaceAll(Pattern.quote("%d%"), destinationFilePath);
		} else if (str.equalsIgnoreCase("%nbproc%")) {
			str3 = "" + Runtime.getRuntime().availableProcessors();
		} else if (str.indexOf("%bn%") >= 0) {
			str3 = str.replaceAll(Pattern.quote("%bn%"), FilenameUtils.getBaseName(nativeFilePath));
		} else if (str.indexOf("%bd%") >= 0) {
			str3 = str.replaceAll(Pattern.quote("%bd%"), FilenameUtils.getBaseName(destinationFilePath));
		} else if (str.startsWith("%") && str.endsWith("%")) {
			final String propname = str.substring(1, str.length() - 1);
			final String propvalue = this.props.getProperty(propname);
			if (propvalue == null) {
				final String errorMess = "Variable " + propname + " is not a defined property";
				this.logger.error(errorMess);
				output.add(str);
			} else {
				// Since property value can be "-timelimit 10", we need to split this value before to add it to the command
				final ArrayList<String> propvalueSplitted = AxesFFmpegNormaliser.splitAndTrim(propvalue);
				for (final String str2 : propvalueSplitted) {
					final ArrayList<String> tab = new ArrayList<>();
					this.replaceRecursively(str2, tab, tempFileNamePrefix, nativeFilePath, destinationFilePath);
					output.addAll(tab);
				}
			}
		} else {
			output.add(str);
		}

		if (str3 != null) {
			final ArrayList<String> tab = new ArrayList<>();
			this.replaceRecursively(str3, tab, tempFileNamePrefix, nativeFilePath, destinationFilePath);
			output.addAll(tab);
		}
	}


	/**
	 * Split and Trim a string into an array of strings trimmed
	 *
	 * @param command
	 * @return array of strings trimed
	 */
	private static ArrayList<String> splitAndTrim(final String command) {
		final ArrayList<String> res = new ArrayList<>();
		final String[] tab = command.split(" ");
		for (final String str : tab) {
			final String trimed = str.trim();
			// Skip multiple blanks
			if (trimed.length() > 0) {
				res.add(trimed);
			}
		}
		return res;
	}


	/**
	 * Return configured video command lines.
	 *
	 * @return The map of command lines per format
	 */
	public Map<String, String> getVideoCmdLines() {
		return this.videoCmdLines;
	}

}
