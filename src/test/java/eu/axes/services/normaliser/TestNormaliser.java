package eu.axes.services.normaliser;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Video;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.purl.dc.elements.DublinCoreAnnotator;
import org.purl.dc.terms.DCTermsAnnotator;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.FileSystemResource;

import eu.axes.components.contentmanager.AxesContentWriter;
import eu.axes.utils.AxesAnnotator;

@SuppressWarnings({ "javadoc", "static-method" })
public class TestNormaliser {


	private static final File FFMPEG = new File("/usr/local/bin/ffmpeg");


	private final Log log = LogFactory.getLog(this.getClass());


	private WebLabMarshaller webLabMarshaller = new WebLabMarshaller();


	private VideoNormaliserService axesFFmpegNormaliser;


	private AxesContentWriter contentManager;


	private HashMap<String, String> mimeMap;


	@Before
	public void init() throws IOException {
		Assume.assumeTrue(SystemUtils.IS_OS_LINUX);
		Assume.assumeTrue(TestNormaliser.FFMPEG.exists() && TestNormaliser.FFMPEG.canExecute());
		this.axesFFmpegNormaliser = new VideoNormaliserService(TestNormaliser.FFMPEG.getPath(), "target/tmp");
		this.contentManager = new AxesContentWriter();
		this.mimeMap = new HashMap<>(this.contentManager.getFormatToExtMap().size());
		for (Entry<String, String> entry : this.contentManager.getFormatToExtMap().entrySet()) {
			this.mimeMap.put(entry.getValue(), entry.getKey());
		}
	}


	@Test
	public void testWithRealCommand() throws Exception {
		for (final File folder : new File("src/test/resources").listFiles((FileFilter) FileFilterUtils.makeSVNAware(FileFilterUtils.directoryFileFilter()))) {
			for (final File videoFile : folder.listFiles((FileFilter) FileFilterUtils.makeSVNAware(FileFilterUtils.fileFileFilter()))) {

				final String path = FilenameUtils.normalize(videoFile.getAbsolutePath());
				this.log.debug("Create AXES resource from file " + path + ".");

				final String originalFilename = videoFile.getName();
				final String videoId = 'v' + FilenameUtils.getBaseName(originalFilename);

				final long size = videoFile.length();

				final String humanReadableSize = FileUtils.byteCountToDisplaySize(size);
				final Date lastModified = new Date(videoFile.lastModified());

				/*
				 * Create the document and annotate it. It uses an uri scheme which is not the same than the original weblab one. Moreover, it contains specific annotations.
				 */
				final Document document = new Document();
				document.setUri("http://axes-project.eu/" + "c" + folder.getName() + "/" + videoId);
				final Annotation documentAnnot = WebLabResourceFactory.createAndLinkAnnotation(document);
				final DublinCoreAnnotator docDCA = this.annotateWithCommonProperties(document, documentAnnot, videoFile, videoId);
				docDCA.writeSource(path);
				final WProcessingAnnotator docWPA = new WProcessingAnnotator(URI.create(document.getUri()), documentAnnot);
				docWPA.writeOriginalFileName(originalFilename);
				docWPA.writeOriginalFileSize(Long.valueOf(size));
				final DCTermsAnnotator dcta = new DCTermsAnnotator(URI.create(document.getUri()), documentAnnot);
				dcta.writeExtent(humanReadableSize);
				dcta.writeModified(lastModified);

				/*
				 * Create the video and annotate it. It uses an uri scheme which is not the same than the original weblab one. Moreover, it contains specific annotations.
				 */
				final Video video = new Video();
				video.setUri(document.getUri() + "#mainVideo");
				document.getMediaUnit().add(video);
				final Annotation videoAnnot = WebLabResourceFactory.createAndLinkAnnotation(video);
				this.annotateWithCommonProperties(video, videoAnnot, videoFile, videoId);

				final ProcessArgs processArgs = new ProcessArgs();
				processArgs.setResource(document);
				final Resource response = this.axesFFmpegNormaliser.process(processArgs).getResource();
				Assert.assertNotNull(response);
				Assert.assertTrue(response.getClass().isAssignableFrom(Document.class));
				final MediaUnit mediaUnit = ((Document) response).getMediaUnit().get(0);
				Assert.assertNotNull(mediaUnit);
				Assert.assertTrue(mediaUnit.getClass().isAssignableFrom(Video.class));
				this.webLabMarshaller.marshalResource(response, new File("target/" + videoId + ".xml"));
			}
		}
	}



	@Test
	public void testLoadingConfiguration() throws Exception {
		final VideoNormaliserService service = new XmlBeanFactory(new FileSystemResource("src/main/webapp/WEB-INF/cxf-servlet.xml")).getBean(VideoNormaliserService.class);
		Assert.assertNotNull(service.axesFFmpegNormaliser.getVideoCmdLines());
		Assert.assertNotEquals(0, service.axesFFmpegNormaliser.getVideoCmdLines().size());
	}


	/**
	 * @param file
	 *            The source file to be stored as native content and from which we shall extract the mime type from the extension.
	 * @param resource
	 *            The resource to be annotated
	 * @param annotation
	 *            The annotation to be enriched
	 * @param videoId
	 *            The video Id
	 * @return The created {@link DublinCoreAnnotator} instance of the resource.
	 * @throws WebLabCheckedException
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	private DublinCoreAnnotator annotateWithCommonProperties(final Resource resource, final Annotation annotation, final File file, final String videoId) throws WebLabCheckedException,
			FileNotFoundException, IOException {
		final AxesAnnotator aa = new AxesAnnotator(URI.create(resource.getUri()), annotation);
		aa.writeCollectionId("c" + file.getParentFile().getName());
		aa.writeVideoId(videoId);

		final String format = this.mimeMap.get(FilenameUtils.getExtension(file.getName()));

		final DublinCoreAnnotator dca = new DublinCoreAnnotator(URI.create(resource.getUri()), annotation);
		dca.writeFormat(format);
		try (FileInputStream fis = new FileInputStream(file)) {
			final URI contentUri = this.contentManager.writeContent(fis, resource);
			dca.startInnerAnnotatorOn(contentUri);
			dca.writeFormat(format);
			dca.endInnerAnnotator();
		}

		return dca;
	}


}
